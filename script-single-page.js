// create the module and name it scotchApp
var scotchApp = angular.module('scotchApp', ['ngRoute'])

// configure our routes
.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('%%').endSymbol('%%');
})
.config(function ($routeProvider) {

    $routeProvider
    // route for the home page
		.when('/', {
			templateUrl : 'pages-single/home.html',
			controller  : 'mainController'
		})

		.when('/index', {
			templateUrl : 'pages-single/index.html',
			controller  : 'indexController'
		})

		.when('/control', {
			templateUrl : 'pages-single/control.html',
			controller  : 'controlController'
		})

		.when('/content', {
			templateUrl : 'pages-single/content.html',
			controller  : 'contentController'
		})
});


scotchApp.controller('menuList', ['$scope', '$http', function(s,h){
	s.checkMenu = 1;

	s.changeMenu = function(id){
		s.checkMenu = id;
	};
}]);

// create the controller and inject Angular's $scope
scotchApp.controller('mainController', function($scope) {
	// create a message to display in our view
	$scope.message = 'This is mainController for main page';
});

scotchApp.controller('indexController', function($scope) {
	// create a message to display in our view
	$scope.message = 'This is indexController for index';
});

scotchApp.controller('controlController', function($scope) {
	// create a message to display in our view
	$scope.message = 'This is controlController for control';
});

scotchApp.controller('contentController', function($scope) {
	// create a message to display in our view
	$scope.message = 'This is controlController for control';
});