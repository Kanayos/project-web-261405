// create the module and name it scotchApp
var scotchApp = angular.module('scotchApp', ['ngRoute'])

// configure our routes
.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('%%').endSymbol('%%');
})
.config(function ($routeProvider) {

    $routeProvider
    // route for the home page
		.when('/', {
			templateUrl : 'pages-comment/tong.html',
			controller  : 'mainController'
		})
		.when('/yost', {
			templateUrl : 'pages-comment/yost.html',
			controller  : 'yostController'
		})
});


scotchApp.controller('menuList', ['$scope', '$http', function(s,h){
	s.checkMenu = 1;

	s.changeMenu = function(id){
		s.checkMenu = id;
	};
}]);

// create the controller and inject Angular's $scope
scotchApp.controller('mainController', function($scope) {
	// create a message to display in our view
	$scope.message = 'Everyone come and see how good I look!';
});

scotchApp.controller('yostController', function($scope) {
	// create a message to display in our view
	$scope.message = 'Everyone come and see how good I look!';
});