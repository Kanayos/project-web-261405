// create the module and name it scotchApp
var scotchApp = angular.module('scotchApp', ['ngRoute'])

// configure our routes
.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('%%').endSymbol('%%');
})
.config(function ($routeProvider) {

    $routeProvider
    // route for the home page
		.when('/', {
			templateUrl : 'pages/install.html',
			controller  : 'mainController'
		})

		// route for the about page
		.when('/intro2', {
			templateUrl : 'pages/intro2.html',
			controller  : 'intro2Controller'
		})

		// route for the contact page
		.when('/modules', {
			templateUrl : 'pages/modules.html',
			controller  : 'modulesController'
		})

		.when('/model', {
			templateUrl : 'pages/model.html',
			controller  : 'modelController'
		})

		.when('/controller', {
			templateUrl : 'pages/controller.html',
			controller  : 'controllerController'
		})

		.when('/filter', {
			templateUrl : 'pages/filter.html',
			controller  : 'filterController'
		})

		.when('/http', {
			templateUrl : 'pages/http.html',
			controller  : 'httpController'
		});
});


scotchApp.controller('menuList', ['$scope', '$http', function(s,h){
	s.checkMenu = 1;

	s.changeMenu = function(id){
		s.checkMenu = id;
	};
}]);

// create the controller and inject Angular's $scope
scotchApp.controller('mainController', function($scope) {
	// create a message to display in our view
	$scope.message = 'Everyone come and see how good I look!';
});

scotchApp.controller('intro2Controller', function($scope) {
	$scope.name = { firstName:'John',lastName:'Doe' };
	$scope.firstName= "John";
	$scope.lastName= "Doe";
});

scotchApp.controller('modulesController', function($scope) {
	$scope.message = 'Contact us! JK. This is just a demo.';
});

scotchApp.controller('modelController', function($scope) {
	$scope.nametest = "John Doe";
});

scotchApp.controller('controllerController', function($scope) {
	$scope.firstName = "John";
	$scope.lastName = "Doe";
	$scope.fullName = function() {
	    return $scope.firstName + " " + $scope.lastName;
	};
});

scotchApp.controller('filterController', function($scope) {
	$scope.firstName = "John";
	$scope.lastName = "Doe";
	$scope.fullName = function() {
	    return $scope.firstName + " " + $scope.lastName;
	};
	$scope.names = [
        {name:'Jani',country:'Norway'},
        {name:'Carl',country:'Sweden'},
        {name:'Margareth',country:'England'},
        {name:'Hege',country:'Norway'},
        {name:'Joe',country:'Denmark'},
        {name:'Gustav',country:'Sweden'},
        {name:'Birgit',country:'Denmark'},
        {name:'Mary',country:'England'},
        {name:'Kai',country:'Norway'}
        ];
    $scope.names2 = [
        'Jani',
        'Carl',
        'Margareth',
        'Hege',
        'Joe',
        'Gustav',
        'Birgit',
        'Mary',
        'Kai'
    ]; 
});

scotchApp.filter('myFormat', function() {
    return function(x) {
        var i, c, txt = "";
        for (i = 0; i < x.length; i++) {
            c = x[i];
            if (i % 2 == 0) {
                c = c.toUpperCase();
            }
            txt += c;
        }
        return txt;
    };
});

scotchApp.controller('httpController', function($scope, $http) {
    $http.get("httpweb/welcome.html")
    .then(function(response) {
        $scope.myWelcome = response.data;
    });
	$scope.url = {name : ''};
    $scope.trig = function (){
   		$http({
   		    method : "GET",
   		    url : $scope.url.name
   		}).then(function mySucces(response) {
   		    $scope.myWelcome2 = response.data;
   		}, function myError(response) {
   		    $scope.myWelcome2 = response.statusText;
   		});
   	}
   	$http({
		method : 'GET',
		url : 'httpweb/welcome.html',
		headers: {
			'Content-Type': 'application/json',
			'Accept': 'application/json'
		}
	})
    .then(function(response) {
        $scope.content = response.data;
        $scope.statuscode = response.status;
        $scope.statustext = response.statustext; 
    });
    $http({
		method : 'GET',
		url : 'httpweb/planet.json',
		headers: {
			'Content-Type': 'application/json',
			'Accept': 'application/json'
		}
	})
    .then(function(response) {
        $scope.myData = response.data;
        console.log($scope.myData);
    });
});