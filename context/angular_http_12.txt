AngularJS AJAX - $http
$http �繺�ԡ�âͧ AngularJS �����㹡����ҹ�����Ũҡ servers
--
$http � AngularJS 
$http �зӡ����ͧ����ѧ server �ҡ��� server ���觡�õͺʹͧ��Ѻ��
///
<div ng-app="myApp" ng-controller="myCtrl"> 

<p>Today's welcome message is:</p>
<h1>{{myWelcome}}</h1>

</div>

<script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http) {
    $http.get("welcome.htm")
    .then(function(response) {
        $scope.myWelcome = response.data;
    });
});
</script>
///
��кǹ��âͧ $http �͡�ҡ .get() ����
.delete()
.head()
.jsonp()
.patch()
.post()
.put()
������ҧ�� 
///
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http) {
    $http({
        method : "GET",
        url : "welcome.htm"
    }).then(function mySucces(response) {
        $scope.myWelcome = response.data;
    }, function myError(response) {
        $scope.myWelcome = response.statusText;
    });
});
///
�ҡ������ҧ���зӡ�û����żŴ��µ���� ��觻�Сͺ���� url, ��觷��зӶ���ҡ����� �����觷��зӶ���ҡ�Դ��Ҵ
----
�س���ѵ�
�ŵͺʹͧ�ҡ�������������ö��˹��س���ѵ���ѧ���
.config �ѵ�ط����㹡�����ҧ����ͧ��
.data �繤� �������ѵ�بҡ���������
.headers �ѧ��ѹ��������Ǣ�ͧ͢������
.status ����Ţ����˹�ʶҹТͧ HTTP
.statusText �ӷ���˹�ʶҹТͧ HTTP
������ҧ
///
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http) {
    $http.get("welcome.htm")
    .then(function(response) {
        $scope.content = response.data;
        $scope.statuscode = response.status;
        $scope.statustext = response.statustext; 
    });
});
///
---
JSON
�����ŷ�����Ѻ�ҡ����������������ٻẺ��� JSON  ������Ըշ���дǡ��Ч��·��� AngularJS ������������  
������ҧ�� �������������觤�Ңͧ�١��� 15 �� ���������  ng-repeat �ѧ���
///
<div ng-app="myApp" ng-controller="customersCtrl"> 

<ul>
  <li ng-repeat="x in myData">
    {{ x.Name + ', ' + x.Country }}
  </li>
</ul>

</div>

<script>
var app = angular.module('myApp', []);
app.controller('customersCtrl', function($scope, $http) {
    $http.get("customers.php").then(function(response) {
        $scope.myData = response.data.records;
    });
});
</script>
///
�;���पѹ��˹���ǤǺ��� customersCtrl ���¤��  $scope ��� $http ��е�ǤǺ����������ҧ����ü�ҹ $scope ���¢����� JSON �ҡ���������