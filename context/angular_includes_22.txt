���������ǹ��Сͺ�ͧ AngularJS 
� AngularJS  �������ö������ǹ��Сͺ�ͧ HTML �ҡ���������������� ng-include
������ҧ 
///
<body ng-app="">

<div ng-include="'myFile.htm'"></div>

</body>
///
��ǹ�ͧ code ���ǹ��Сͺ
������� HTML �����Ҩ������������� ng-include ��� ����ö��¹�� AngularJS ��ѧ���
������ҧ��� myTable.htm
///
<table>
<tr ng-repeat="x in names">
<td>{{ x.Name }}</td>
<td>{{ x.Country }}</td>
</tr>
</table>
///
�������ѧ��������纫�� code �����鹡�ж١���Թ��ô��� ������� code �ѧ���
///
<body>

<div ng-app="myApp" ng-controller="customersCtrl"> 
  <div ng-include="'myTable.htm'"></div>
</div>

<script>
var app = angular.module('myApp', []);
app.controller('customersCtrl', function($scope, $http) {
    $http.get("customers.php").then(function (response) {
        $scope.names = response.data.records;
    });
});
</script>
///
---
�ͺࢵ�ͧ���������ǹ��Сͺ
�������á��� ����� ng-include ���������������ҡ�ͺࢵ���� 
�����������ҡ�ͺࢵ��蹹������ö�����������͹حҵ����ҧ�١��ͧ(whitelist)㹿ѧ��ѹ��õ�駤����;���पѹ
������ҧ
///
<body ng-app="myApp">

<div ng-include="'http://www.refsnesdata.no/angular_include.asp'"></div>

<script>
var app = angular.module('myApp', [])
app.config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        'http://www.refsnesdata.no/**'
    ]);
});
</script>

</body>
///



